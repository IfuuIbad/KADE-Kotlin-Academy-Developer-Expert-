package me.ifuuibad.kadefootballmanager.ui.match.lastmatch

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.ifuuibad.kadefootballmanager.data.model.Match
import me.ifuuibad.kadefootballmanager.ui.match.MatchView
import me.ifuuibad.kadefootballmanager.utils.CoroutineContextProviderTest
import me.ifuuibad.kadefootballmanager.utils.NetworkService
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LastMatchPresenterTest{
    @Mock
    private lateinit var view: MatchView
    private lateinit var network: NetworkService
    private lateinit var presenter: LastMatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        network = NetworkService()
        presenter = LastMatchPresenter(network, CoroutineContextProviderTest(), view)
    }

    @Test
    fun getMatchTest() {
        GlobalScope.launch {
            val matchs: MutableList<Match> = mutableListOf()
            val league = "English Premier League"

            presenter.getMatch(league)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showMatchList(matchs)
            Mockito.verify(view).hideLoading()
        }
    }
}