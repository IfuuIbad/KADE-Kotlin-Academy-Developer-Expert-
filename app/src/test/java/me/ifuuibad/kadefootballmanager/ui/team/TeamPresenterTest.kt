package me.ifuuibad.kadefootballmanager.ui.team

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.ifuuibad.kadefootballmanager.data.model.Team
import me.ifuuibad.kadefootballmanager.utils.CoroutineContextProviderTest
import me.ifuuibad.kadefootballmanager.utils.NetworkService
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamPresenterTest{
    @Mock
    private lateinit var view: TeamView
    private lateinit var network: NetworkService
    private lateinit var presenter: TeamPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        network = NetworkService()
        presenter = TeamPresenter(network, CoroutineContextProviderTest(), view)
    }

    @Test
    fun getTeamTest(){
        GlobalScope.launch {
            val teams: MutableList<Team> = mutableListOf()
            val league = "English Premier League"
            presenter.getTeam(league)

            Mockito.verify(view).showLoading()
            Mockito.verify(view).showTeamList(teams)
            Mockito.verify(view).hideLoading()
        }
    }
}