package me.ifuuibad.kadefootballmanager.utils

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import me.ifuuibad.kadefootballmanager.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NetworkService{
    fun okHttpClient(): OkHttpClient{
        val client = OkHttpClient().newBuilder()
        val interceptor = Interceptor {chain ->
            val request = chain.request()?.newBuilder()?.build()

            val response = chain.proceed(request)

            if(response.code().equals(403)){
                print("network error ..")
            }

            response
        }

        client.networkInterceptors().add(interceptor)
        return client.build()
    }

    fun gsonBuilder(): Gson{
        return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setLenient()
                .create()
    }

    fun retrofitBuilder(): Retrofit {
        return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                .client(this.okHttpClient())
                .addConverterFactory(GsonConverterFactory.create(this.gsonBuilder()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}