package me.ifuuibad.kadefootballmanager.utils

import java.text.SimpleDateFormat
import java.util.*

fun convertDateTime(date: String?, time: String?): Date? {
    val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    format.timeZone = TimeZone.getTimeZone("UTC")
    val dateTime = "$date $time"
    return format.parse(dateTime)
}