package me.ifuuibad.kadefootballmanager.ui.team


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_team.*
import me.ifuuibad.kadefootballmanager.R
import me.ifuuibad.kadefootballmanager.R.array.league
import me.ifuuibad.kadefootballmanager.data.model.Team
import me.ifuuibad.kadefootballmanager.utils.CoroutineContextProvider
import me.ifuuibad.kadefootballmanager.utils.NetworkService
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.toast

class TeamFragment : Fragment(), TeamView {
    private lateinit var leagueName: String
    private lateinit var adapter: TeamAdapter
    private lateinit var contextProvider: CoroutineContextProvider
    private lateinit var presenter: TeamPresenter
    private var listTeam: MutableList<Team> = mutableListOf()
    private var filteredList: MutableList<Team> = mutableListOf()

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val spinnerItems = resources.getStringArray(league)
        val spinnerAdapter = ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item, spinnerItems)

        contextProvider = CoroutineContextProvider()
        presenter = TeamPresenter(NetworkService(), contextProvider, this)

        spinner.adapter = spinnerAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                leagueName = spinner.selectedItem.toString()
                presenter.getTeam(leagueName)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        adapter = TeamAdapter(filteredList, object : TeamAdapter.onClickListener {
            override fun onClick(view: View, team: Team) {
                toast("cliked")
            }
        })

        rv_team.layoutManager = LinearLayoutManager(activity)
        rv_team.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        inflater?.inflate(R.menu.search_menu, menu)
        val searchItem = menu?.findItem(R.id.menu_search)

        if (searchItem != null){
            val searchView = searchItem.actionView as SearchView
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(p0: String?): Boolean {


                    return true
                }

                override fun onQueryTextChange(filter: String?): Boolean {
                    if (filter!!.isNotEmpty()){
                        filteredList.clear()

                        val search = filter.toLowerCase()
                        listTeam.forEach{
                            if (it.strTeam!!.toLowerCase().contains(search)){
                                filteredList.add(it)
                                adapter.notifyDataSetChanged()
                            }
                        }

                    }else{
                        filteredList.clear()
                        filteredList.addAll(listTeam)
                        adapter.notifyDataSetChanged()
                    }

                    return true
                }

            })
        }

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_team, container, false)
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showTeamList(data: List<Team>) {
        listTeam.clear()
        filteredList.clear()
        filteredList.addAll(data)
        listTeam.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun showError(message: String) {
        rv_team.snackbar(message)
    }

}
