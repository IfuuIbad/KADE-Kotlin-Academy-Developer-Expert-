package me.ifuuibad.kadefootballmanager.ui.team

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.content_team.*
import me.ifuuibad.kadefootballmanager.R
import me.ifuuibad.kadefootballmanager.data.model.Team

class TeamAdapter(
        private val listItem: MutableList<Team>,
        private val listener: onClickListener
) : RecyclerView.Adapter<TeamAdapter.TeamViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): TeamViewHolder {
        return TeamViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.content_team, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItem(listItem[position])
    }

    inner class TeamViewHolder(
            override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        @SuppressLint("NewApi")
        fun bindItem(team: Team) {
            Glide.with(containerView).load(team.strTeamBadge).into(iv_teamBadge)
            txt_teamName.text = team.strTeam
            containerView.setOnClickListener { listener.onClick(it, team) }
        }
    }

    interface onClickListener {
        fun onClick(view: View, team: Team)
    }

}