package me.ifuuibad.kadefootballmanager.ui.match.lastmatch

import android.annotation.SuppressLint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.ifuuibad.kadefootballmanager.data.api.ApiRepository
import me.ifuuibad.kadefootballmanager.ui.match.MatchView
import me.ifuuibad.kadefootballmanager.utils.CoroutineContextProvider
import me.ifuuibad.kadefootballmanager.utils.InternetChecker
import me.ifuuibad.kadefootballmanager.utils.NetworkService

class LastMatchPresenter(
        private val network: NetworkService,
        private val contextProvider: CoroutineContextProvider = CoroutineContextProvider(),
        private val view: MatchView
)
{
    fun getMatch(league: String){
        GlobalScope.launch(contextProvider.main) {
            view.showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                @SuppressLint("CheckResult")
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.retrofitBuilder()
                                .create(ApiRepository::class.java)
                                .matchLastLeague(league)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    view.showMatchList(it.matchs)
                                    view.hideLoading()
                                }, {
                                    error(it)
                                })
                    } else {
                        view.showError("You haven't Internet Connection! ..")
                        view.hideLoading()
                    }
                }
            })
        }
    }
}