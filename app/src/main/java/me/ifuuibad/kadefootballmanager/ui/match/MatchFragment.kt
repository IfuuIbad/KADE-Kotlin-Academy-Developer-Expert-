package me.ifuuibad.kadefootballmanager.ui.match

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_match.*
import me.ifuuibad.kadefootballmanager.R
import me.ifuuibad.kadefootballmanager.ui.match.lastmatch.LastMatchFragment
import me.ifuuibad.kadefootballmanager.ui.match.nextmatch.NextMatchFragment
import me.ifuuibad.kadefootballmanager.utils.ViewPagerAdapter

class MatchFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)

        super.onViewCreated(view, savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_match, container, false)
    }

    private fun initViewPager(vPager: ViewPager) {
        val vAdapter = ViewPagerAdapter(childFragmentManager)
        vAdapter.addFrag(NextMatchFragment(), "Next Match")
        vAdapter.addFrag(LastMatchFragment(), "Last Match")
        vPager.adapter = vAdapter
    }

}
