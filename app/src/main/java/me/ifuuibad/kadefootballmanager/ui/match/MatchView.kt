package me.ifuuibad.kadefootballmanager.ui.match

import me.ifuuibad.kadefootballmanager.data.model.Match

interface MatchView {
    fun showLoading()
    fun hideLoading()
    fun showMatchList(data: List<Match>)
    fun showError(message: String)
}