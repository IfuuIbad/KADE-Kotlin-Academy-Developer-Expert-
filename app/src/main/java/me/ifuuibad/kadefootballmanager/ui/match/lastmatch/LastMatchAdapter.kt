package me.ifuuibad.kadefootballmanager.ui.match.lastmatch

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.content_last_match.*
import me.ifuuibad.kadefootballmanager.R
import me.ifuuibad.kadefootballmanager.data.model.Match
import me.ifuuibad.kadefootballmanager.utils.convertDateTime
import java.text.SimpleDateFormat
import java.util.*

class LastMatchAdapter(
        private val listItem: MutableList<Match>,
        private val listener: onClickListener
): RecyclerView.Adapter<LastMatchAdapter.LastMatchViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): LastMatchViewHolder {
        return LastMatchViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.content_last_match, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: LastMatchViewHolder, position: Int) {
        holder.bindItem(listItem[position])
    }

    inner class LastMatchViewHolder(
            override val containerView: View) : RecyclerView.ViewHolder(containerView),
            LayoutContainer {

        @SuppressLint("NewApi")
        fun bindItem(match: Match) {
            val formatDate = SimpleDateFormat("EEE, dd MMM yyyy", Locale.getDefault())
            val formatTime = SimpleDateFormat("HH:mm", Locale.getDefault())
            val confertDateTime = convertDateTime(match.dateEvent, match.strTime)

            txt_date.text = formatDate.format(confertDateTime)
            txt_hour.text = formatTime.format(confertDateTime)
            txt_away.text = match.strAwayTeam
            txt_home.text = match.strHomeTeam
            txt_scoreAway.text = match.intAwayScore
            txt_scoreHome.text = match.intHomeScore

            containerView.setOnClickListener { listener.onClick(it, match) }
        }
    }

    interface onClickListener {
        fun onClick(view: View, match: Match)
    }
}