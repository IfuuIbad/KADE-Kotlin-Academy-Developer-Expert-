package me.ifuuibad.kadefootballmanager.ui.match.nextmatch

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_next_match.*
import me.ifuuibad.kadefootballmanager.R
import me.ifuuibad.kadefootballmanager.R.array.league
import me.ifuuibad.kadefootballmanager.data.model.Match
import me.ifuuibad.kadefootballmanager.ui.match.MatchView
import me.ifuuibad.kadefootballmanager.utils.CoroutineContextProvider
import me.ifuuibad.kadefootballmanager.utils.NetworkService
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.toast


class NextMatchFragment : Fragment(), MatchView {

    private lateinit var presenter: NextMatchPresenter
    private lateinit var contextProvider: CoroutineContextProvider
    private lateinit var idLeague: String
    private lateinit var adapter: NextMatchAdapter
    private var filteredList: MutableList<Match> = mutableListOf()
    private var listMatch: MutableList<Match> = mutableListOf()

    init {
        setHasOptionsMenu(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val spinnerItems = resources.getStringArray(league)
        val spinnerAdapter = ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item, spinnerItems)

        contextProvider = CoroutineContextProvider()
        presenter = NextMatchPresenter(NetworkService(), contextProvider, this)

        spinner.adapter = spinnerAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        idLeague = "4328"
                        presenter.getMatch(idLeague)
                    }
                    1 -> {
                        idLeague = "4329"
                        presenter.getMatch(idLeague)
                    }
                    2 -> {
                        idLeague = "4331"
                        presenter.getMatch(idLeague)
                    }
                    3 -> {
                        idLeague = "4332"
                        presenter.getMatch(idLeague)
                    }
                    4 -> {
                        idLeague = "4334"
                        presenter.getMatch(idLeague)
                    }
                    5 -> {
                        idLeague = "4335"
                        presenter.getMatch(idLeague)
                    }
                }
            }
        }

        adapter = NextMatchAdapter(filteredList, object : NextMatchAdapter.onClickListener {
            override fun onClick(view: View, match: Match) {
                toast("cliked")
            }
        })

        rv_next.layoutManager = LinearLayoutManager(activity)
        rv_next.adapter = adapter

        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        inflater?.inflate(R.menu.search_menu, menu)
        val searchItem = menu?.findItem(R.id.menu_search)

        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(p0: String?): Boolean {


                    return true
                }

                override fun onQueryTextChange(filter: String?): Boolean {
                    if (filter!!.isNotEmpty()) {
                        filteredList.clear()

                        val search = filter.toLowerCase()
                        listMatch.forEach {
                            if (it.strHomeTeam!!.toLowerCase().contains(search) ||
                                    it.strAwayTeam!!.toLowerCase().contains(search)) {
                                filteredList.add(it)
                                adapter.notifyDataSetChanged()
                            }
                        }
                    } else {
                        filteredList.clear()
                        filteredList.addAll(listMatch)
                        adapter.notifyDataSetChanged()
                    }

                    return true
                }

            })
        }

        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_next_match, container, false)
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showMatchList(data: List<Match>) {
        listMatch.clear()
        filteredList.clear()
        listMatch.addAll(data)
        filteredList.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun showError(message: String) {
        rv_next.snackbar(message)

    }
}
