package me.ifuuibad.kadefootballmanager.ui.detail

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import me.ifuuibad.kadefootballmanager.R

class TeamDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_detail)
    }
}
