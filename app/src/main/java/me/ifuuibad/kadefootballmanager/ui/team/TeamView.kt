package me.ifuuibad.kadefootballmanager.ui.team

import me.ifuuibad.kadefootballmanager.data.model.Team

interface TeamView{
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
    fun showError(message: String)
}