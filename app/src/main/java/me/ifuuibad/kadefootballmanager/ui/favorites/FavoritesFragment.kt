package me.ifuuibad.kadefootballmanager.ui.favorites


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_match.*
import me.ifuuibad.kadefootballmanager.R
import me.ifuuibad.kadefootballmanager.ui.favorites.match.FavoritesMatchFragment
import me.ifuuibad.kadefootballmanager.ui.favorites.team.FavoritesTeamFragment
import me.ifuuibad.kadefootballmanager.utils.ViewPagerAdapter

class FavoritesFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    private fun initViewPager(vPager: ViewPager) {
        val vAdapter = ViewPagerAdapter(childFragmentManager)
        vAdapter.addFrag(FavoritesMatchFragment(), "Match")
        vAdapter.addFrag(FavoritesTeamFragment(), "Team")
        vPager.adapter = vAdapter
    }

}
