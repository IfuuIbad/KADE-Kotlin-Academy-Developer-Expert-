package me.ifuuibad.kadefootballmanager.ui.match.nextmatch

import android.annotation.SuppressLint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import me.ifuuibad.kadefootballmanager.data.api.ApiRepository
import me.ifuuibad.kadefootballmanager.ui.match.MatchView
import me.ifuuibad.kadefootballmanager.utils.CoroutineContextProvider
import me.ifuuibad.kadefootballmanager.utils.InternetChecker
import me.ifuuibad.kadefootballmanager.utils.NetworkService

class NextMatchPresenter(
        private val network: NetworkService,
        private val contextProvider: CoroutineContextProvider = CoroutineContextProvider(),
        private val view: MatchView
        )
{
    fun getMatch(league: String){
        GlobalScope.launch(contextProvider.main) {
            view.showLoading()
            InternetChecker(object : InternetChecker.Consumer {
                @SuppressLint("CheckResult")
                override fun accept(internet: Boolean) {
                    if (internet) {
                        network.retrofitBuilder()
                                .create(ApiRepository::class.java)
                                .matchNextLeague(league)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    view.showMatchList(it.matchs)
                                    view.hideLoading()
                                }, {
                                    error(it)
                                })
                    } else {
                        view.showError("You haven't Internet Connection! ..")
                        view.hideLoading()
                    }
                }
            })
        }
    }
}