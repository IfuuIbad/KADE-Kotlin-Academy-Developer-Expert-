package me.ifuuibad.kadefootballmanager.ui.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.Menu
import kotlinx.android.synthetic.main.activity_main.*
import me.ifuuibad.kadefootballmanager.R
import me.ifuuibad.kadefootballmanager.ui.favorites.FavoritesFragment
import me.ifuuibad.kadefootballmanager.ui.match.MatchFragment
import me.ifuuibad.kadefootballmanager.ui.match.lastmatch.LastMatchFragment
import me.ifuuibad.kadefootballmanager.ui.match.nextmatch.NextMatchFragment
import me.ifuuibad.kadefootballmanager.ui.team.TeamFragment
import me.ifuuibad.kadefootballmanager.utils.ViewPagerAdapter

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.matchs -> {
                    loadMatchFragment(savedInstanceState)
                }
                R.id.teams -> {
                    loadTeamFragment(savedInstanceState)
                }
                R.id.favorites -> {
                    loadFavFragment(savedInstanceState)
                }
            }
            true
        }
        bottom_navigation.selectedItemId = R.id.matchs
    }

    private fun loadMatchFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, MatchFragment(), MatchFragment::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadTeamFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, TeamFragment(), TeamFragment::class.java.simpleName)
                    .commit()
        }
    }

    private fun loadFavFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main_container, FavoritesFragment(), FavoritesFragment::class.java.simpleName)
                    .commit()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.search_menu, menu)
//        return super.onCreateOptionsMenu(menu)
//    }
}
