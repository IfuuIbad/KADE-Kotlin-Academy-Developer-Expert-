package me.ifuuibad.kadefootballmanager.data.model.response

import com.google.gson.annotations.SerializedName
import me.ifuuibad.kadefootballmanager.data.model.Match

data class MatchResponse(
        @SerializedName("events")
        val matchs : List<Match>
)