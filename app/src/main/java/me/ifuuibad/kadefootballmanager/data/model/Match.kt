package me.ifuuibad.kadefootballmanager.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.sql.Time
import java.util.*

@Parcelize
data class Match(

        @SerializedName("idEvent")
        var idEvent : String?,

        @SerializedName("dateEvent")
        var dateEvent : String?,

        @SerializedName("strTime")
        var strTime : String?,

        @SerializedName("idHomeTeam")
        var idHomeTeam : String?,

        @SerializedName("idAwayTeam")
        var idAwayTeam : String?,

        @SerializedName("strHomeTeam")
        var strHomeTeam : String?,

        @SerializedName("strAwayTeam")
        var strAwayTeam : String?,

        @SerializedName("intHomeScore")
        var intHomeScore : String?,

        @SerializedName("intAwayScore")
        var intAwayScore : String?


) : Parcelable
//{
//
//        companion object {
//                const val TABLE_FAVORITE: String = "TABLE_FAVORITE"
//                const val ID: String = "ID_"
//                const val ID_EVENT: String = "ID_EVENT"
//                const val DATE: String = "DATE_"
//                const val HOME_ID: String = "HOME_ID"
//                const val AWAY_ID: String = "AWAY_ID"
//                const val HOME_NAME: String = "HOME_NAME"
//                const val AWAY_NAME: String = "AWAY_NAME"
//                const val HOME_SCORE: String = "HOME_SCORE"
//                const val AWAY_SCORE: String = "AWAY_SCORE"
//                const val HOME_RED: String = "HOME_RED"
//                const val AWAY_RED: String = "AWAY_RED"
//                const val HOME_YELLOW: String = "HOME_YELLOW"
//                const val AWAY_YELLOW: String = "AWAY_YELLOW"
//                const val HOME_GOAL: String = "HOME_GOAL"
//                const val AWAY_GOAL: String = "AWAY_GOAL"
//
//        }
//}