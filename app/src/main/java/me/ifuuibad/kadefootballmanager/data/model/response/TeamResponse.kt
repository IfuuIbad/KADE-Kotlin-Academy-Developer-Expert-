package me.ifuuibad.kadefootballmanager.data.model.response

import com.google.gson.annotations.SerializedName
import me.ifuuibad.kadefootballmanager.data.model.Team

data class TeamResponse(
        @SerializedName("teams")
        var teams: List<Team>
)