package me.ifuuibad.kadefootballmanager.data.model

import com.google.gson.annotations.SerializedName


data class Team(
        @SerializedName("idTeam")
        var idTeam: String?,

        @SerializedName("strTeam")
        var strTeam: String?,

        @SerializedName("strTeamShort")
        var strTeamShort: String?,

        @SerializedName("strTeamBadge")
        var strTeamBadge: String?
)