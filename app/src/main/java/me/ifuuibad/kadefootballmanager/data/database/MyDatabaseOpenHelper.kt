//package me.ifuuibad.kadefootballmanager.data.database
//
//import android.content.Context
//import android.database.sqlite.SQLiteDatabase
//import me.ifuuibad.kadefootballmanager.data.model.Match
//import org.jetbrains.anko.db.*
//
//class MyDatabaseOpenHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FavoriteTeam.db", null, 1) {
//    companion object {
//        private var instance: MyDatabaseOpenHelper? = null
//
//        @Synchronized
//        fun getInstance(ctx: Context): MyDatabaseOpenHelper {
//            if (instance == null) {
//                instance = MyDatabaseOpenHelper(ctx.applicationContext)
//            }
//            return instance as MyDatabaseOpenHelper
//        }
//    }
//
//    override fun onCreate(db: SQLiteDatabase) {
//        // Here you create tables
//        db.createTable(Match.TABLE_FAVORITE, true,
//                Match.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
//                Match.ID_EVENT to TEXT,
//                Match.DATE to TEXT,
//                Match.HOME_ID to TEXT,
//                Match.AWAY_ID to TEXT,
//                Match.HOME_NAME to TEXT,
//                Match.AWAY_NAME to TEXT,
//                Match.HOME_SCORE to TEXT,
//                Match.AWAY_SCORE to TEXT,
//                Match.HOME_RED to TEXT,
//                Match.AWAY_RED to TEXT,
//                Match.HOME_YELLOW to TEXT,
//                Match.AWAY_YELLOW to TEXT,
//                Match.HOME_GOAL to TEXT,
//                Match.AWAY_GOAL to TEXT)
//    }
//
//    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
//        // Here you can upgrade tables, as usual
//        db.dropTable(Match.TABLE_FAVORITE, true)
//    }
//}
//
//// Access property for Context
//val Context.database: MyDatabaseOpenHelper
//    get() = MyDatabaseOpenHelper.getInstance(applicationContext)