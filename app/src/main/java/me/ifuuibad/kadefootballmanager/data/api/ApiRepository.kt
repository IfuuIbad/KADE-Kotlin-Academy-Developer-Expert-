package me.ifuuibad.kadefootballmanager.data.api

import io.reactivex.Observable
import me.ifuuibad.kadefootballmanager.BuildConfig
import me.ifuuibad.kadefootballmanager.data.model.response.TeamResponse
import me.ifuuibad.kadefootballmanager.data.model.response.MatchResponse
import retrofit2.http.*

interface ApiRepository{

    @GET("/api/v1/json/"+BuildConfig.API_KEY+"/eventsnextleague.php")
    fun matchNextLeague(
            @Query("id") idLeague : String
    ) : Observable<MatchResponse>

    @GET("/api/v1/json/"+BuildConfig.API_KEY+"/eventspastleague.php")
    fun matchLastLeague(
           @Query("id") idLeague: String
    ) : Observable<MatchResponse>

    @GET("/api/v1/json/"+BuildConfig.API_KEY+"/lookupteam.php")
    fun clubDetail(
            @Query("id") idTeam : String
    ) : Observable<TeamResponse>

    @GET("/api/v1/json/"+BuildConfig.API_KEY+"/search_all_teams.php")
    fun teamsByLeague(
            @Query("l") league: String
    ) : Observable<TeamResponse>

}